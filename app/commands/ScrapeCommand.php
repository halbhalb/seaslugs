<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ScrapeCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'seaslugs:scrape';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get the list of slugs from the website';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$client = new Goutte\Client();
		$rootUrl = Config::get('seaslugs.root_url');
		$fullIndexUrl = $rootUrl.Config::get('seaslugs.index_url');
		$crawler = $client->request('GET', $fullIndexUrl);
		$crawler->filter('div#content-main ul li')->each(function ($node) use ($rootUrl) {
			$slugName = $node->text();
			$fullUrl = $rootUrl.$node->filter('a')->attr('href');
			$count = Slug::where('url', '=', $fullUrl)->count();
			if ($count>0) {
				return;
			}
			$slug = new Slug;
			$slug->name = $slugName;
			$slug->url = $fullUrl;
			$slug->save();
   			print "$slugName :$fullUrl \n";

		});
		exit;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
