<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TweetCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'seaslugs:tweet';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Send a tweet.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$slug = Slug::findRandomUntweeted();
		if ($slug) {
			echo($slug->url."\n");
			$this->tweetSlug($slug->url);
			$slug->tweeted=1;
			$slug->save(); 
		}
		//$this->tweetSlug('http://seaslugforum.net/find/phanalbo');
		
    	exit;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

	protected function tweetSlug($fullUrl) 
	{
		$client = new Goutte\Client();
		$rootUrl = Config::get('seaslugs.root_url');
		$crawler = $client->request('GET', $fullUrl);
		$slugName = $crawler->filter('div#content-main h1 em')->text();
		$getImageFromTag = function ($node) use ($rootUrl)  {
			$fullUrl = $rootUrl.$node->attr('src');
			return file_get_contents($fullUrl);
		};
		$slugImages1  = $crawler->filter('div#content-main div.imgright img')->each($getImageFromTag);
		$slugImages2  = $crawler->filter('div#content-main img.imgright')->each($getImageFromTag);
		$slugImages = array_merge($slugImages1, $slugImages2);
		$tweetText = "{$slugName} {$fullUrl}";
		Twitter::postTweetMedia(
	        array(
	            'status' => $tweetText,
	            'media[]' =>  $slugImages,
	            'format' => 'json'
	        )
		);
	}

}
