<?php

class Slug extends \Eloquent {
	protected $guarded = ['id'];

	public static function findRandomUntweeted() {
		$slug = self::where('tweeted', '=', 0)->orderByRaw('RAND()')->first();
		return $slug;
	}
}